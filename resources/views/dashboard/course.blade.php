<!DOCTYPE html>

<html lang="ar"

      class="light-style layout-menu-fixed"
      dir="ltr"
      data-theme="theme-default"
      data-assets-path="../assets/"
      data-template="vertical-menu-template-free"
>
<head>
    <meta charset="utf-8"/>
    <meta
        name="viewport"
        content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0"/>
    <style>
        .button {
            border: none;
            color: white;
            padding: 16px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            margin-left: 40%;
            transition-duration: 0.4s;
            cursor: pointer;
        }
        .button2 {
            background-color: white;
            color: black;
            border: 2px solid red;

        }

        .button2:hover {
            background-color: red;
            color: white;
        }
    </style>
    <title>Data Literacy</title>

    <meta name="description" content=""/>

    <!-- Favicon -->
    <link rel="icon" type="image/x-icon" href="{{asset('/assets/img/favicon/favicon.ico')}}"/>

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com"/>
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin/>
    <link
        href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap"
        rel="stylesheet"/>

    <!-- Icons. Uncomment required icon fonts -->
    <link rel="stylesheet" href="{{asset('/assets/vendor/fonts/boxicons.css')}}"/>

    <!-- Core CSS -->
    <link rel="stylesheet" href="{{asset('/assets/vendor/css/core.css')}}" class="template-customizer-core-css"/>
    <link rel="stylesheet" href="{{asset('/assets/vendor/css/theme-default.css')}}"
          class="template-customizer-theme-css"/>
    <link rel="stylesheet" href="{{asset('/assets/css/demo.css')}}"/>

    <!-- Vendors CSS -->
    <link rel="stylesheet" href="{{asset('/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.css')}}"/>

    <link rel="stylesheet" href="{{asset('/assets/vendor/libs/apex-charts/apex-charts.css')}}"/>

    <!-- Page CSS -->

    <!-- Helpers -->
    <script src="{{asset('/assets/vendor/js/helpers.js')}}"></script>

    <!--! Template customizer & Theme config files MUST be included after core stylesheets and helpers.js in the <head> section -->
    <!--? Config:  Mandatory theme config file contain global vars & default theme options, Set your preferred theme option in this file.  -->
    <script src="{{asset('/assets/js/config.js')}}"></script>
</head>

<body>
<!-- Layout wrapper -->
<div class="layout-wrapper layout-content-navbar">
    <div class="layout-container">
        <!-- Menu -->

        <aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme">
            {{--            <div alt="Avatar" style="background-image:url({{asset('assets/img/layouts/oh.jpg')}}) ;width: 100%;height: 50%;"--}}
            {{--                 >--}}
            {{--                <h2 style=" text-align: center; margin-top: 25%; color: black">name coures</h2>--}}
            {{--            </div>--}}
            <div class="container">
                <img src="{{asset('assets/img/layouts/oh.jpg')}}" class="image">
                <div class="middle">
                    <div class="text">Data Management</div>
                </div>
            </div>
            <ul class="menu-inner py-1" dir="ltr">
                <!-- Dashboard -->


                <li class="menu-item ">
                    <a href="/" class="menu-link">
                        <i class="menu-icon tf-icons bx bx-home-circle" style="color:#697a8d;"></i>
                        <div style="color: #697a8d" data-i18n="Analytics">home</div>
                    </a>
                </li>
                <li class="menu-item active">
                    <a href="/course" class="menu-link">
                        <i class="menu-icon tf-icons bx bx-bar-chart"></i>
                        <div data-i18n="Analytics">Welcome</div>
                    </a>
                </li>

                <li class="menu-item ">
                    <a href="/IM_introduction" class="menu-link">
                        <i class="menu-icon tf-icons bx bx-bar-chart" style="color: #697a8d"></i>
                        <div style="color: #697a8d" data-i18n="Analytics">IM introduction</div>
                    </a>
                </li>
                <li class="menu-item ">
                    <a href="/Data_Collection" class="menu-link">
                        <i class="menu-icon tf-icons bx bx-bar-chart" style="color: #697a8d"></i>
                        <div style="color: #697a8d" data-i18n="Analytics">Data Collection</div>
                    </a>
                </li>
                <li class="menu-item ">
                    <a href="/IM_introduction" class="menu-link">
                        <i class="menu-icon tf-icons bx bx-bar-chart" style="color: #697a8d"></i>
                        <div style="color: #697a8d" data-i18n="Analytics">ODK Mobile data collection
                        </div>
                    </a>
                </li>
                <li class="menu-item ">
                    <a href="/IM_introduction" class="menu-link">
                        <i class="menu-icon tf-icons bx bx-bar-chart" style="color: #697a8d"></i>
                        <div style="color: #697a8d" data-i18n="Analytics">Data Management</div>
                    </a>
                </li>
                <li class="menu-item ">
                    <a href="/IM_introduction" class="menu-link">
                        <i class="menu-icon tf-icons bx bx-bar-chart" style="color: #697a8d"></i>
                        <div style="color: #697a8d" data-i18n="Analytics">Data Analysis</div>
                    </a>
                </li>
                <li class="menu-item ">
                    <a href="/IM_introduction" class="menu-link">
                        <i class="menu-icon tf-icons bx bx-bar-chart" style="color: #697a8d"></i>
                        <div style="color: #697a8d" data-i18n="Analytics">GIS</div>
                    </a>
                </li>
                <li class="menu-item ">
                    <a href="/IM_introduction" class="menu-link">
                        <i class="menu-icon tf-icons bx bx-bar-chart" style="color: #697a8d"></i>
                        <div style="color: #697a8d" data-i18n="Analytics">Data Visualization</div>
                    </a>
                </li>
                <li class="menu-item ">
                    <a href="/IM_introduction" class="menu-link">
                        <i class="menu-icon ti-face-sad bx bx-bar-chart" style="color: #697a8d"></i>
                        <div style="color: #697a8d" data-i18n="Analytics">Reporting</div>
                    </a>
                </li>
                <li class="menu-item ">
                    <a href="/IM_introduction" class="menu-link">
                        <i class="menu-icon tf-icons bx bx-bar-chart" style="color: #697a8d"></i>
                        <div style="color: #697a8d" data-i18n="Analytics">MIS</div>
                    </a>
                </li>
                <li class="menu-item ">
                    <a href="/IM_introduction" class="menu-link">
                        <i class="menu-icon tf-icons bx bx-bar-chart" style="color: #697a8d"></i>
                        <div style="color: #697a8d" data-i18n="Analytics">Information sharing</div>
                    </a>
                </li>


            </ul>
        </aside>
        <!-- / Menu -->

        <!-- Layout container -->
        <div class="layout-page">
            <!-- Navbar -->

            <nav
                class="layout-navbar container-xxl navbar navbar-expand-xl navbar-detached align-items-center bg-navbar-theme d-xl-none "
                id="layout-navbar"
            >
                <div class=" layout-menu-toggle navbar-nav align-items-xl-center me-3 me-xl-0 d-xl-none">
                    <a class=" nav-item nav-link px-0 me-xl-4" href="javascript:void(0)">
                        <i class="bx bx-menu bx-sm"></i>
                    </a>
                </div>

            </nav>

            <!-- / Navbar -->

            <!-- Content wrapper -->
            <div class="content-wrapper">
                <!-- Content -->

                <div class="container-xxl flex-grow-1 container-p-y">
                    <div class="row num1">
                        <!-- Total Revenue -->
                        <div class="col-12 col-lg-12 order-2 order-md-3 order-lg-2 mb-4">
                            <div class="card">
                                <div class="row row-bordered g-0">
                                    <div class="col-md-12">
                                        <h1 style="color: red" class=" card-header m-0 me-2 pb-3 text-md-center">Welcome
                                            Module</h1>
                                        <h5 class="card-header m-0 me-2 pb-3 text-md-center">Lesson :1 </h5>
                                        <h5 class="card-header m-0 me-2 pb-3 text-md-center" style="color: red">
                                            Data management basics training</h5>

                                        <div id="" class="px-3">
                                            <img style=" border-color: red ; max-height: 100%; max-width: 100%;"
                                                 src="{{asset('/assets/img/layouts/Untitled-1.svg')}}">

                                        </div>
                                        <div class="noOutline" data-ba="lessonEdit.block" data-ba-index="1"
                                             data-block-id="ckzb0ea1l0083376pdm9hksfo">
                                            <div>
                                                <div class="block-text block-text--onecol "
                                                     style="background-color: rgb(245, 245, 245); padding-top: 10px; padding-bottom: 20px;">
                                                    <div class="block-text__container padeng"
                                                         data-ba="blocks.blockText">
                                                        <div class="block-text__row">
                                                            <div class="block-text__col brand--head ">

                                                                <h2>
                                                                    <div class=" brand--linkColor"
                                                                         style="   padding-top:5%;">
                                                                        <div class="fr-view"><p><strong><span
                                                                                        style="font-size:24px;color:rgb(49, 53, 55);"><strong>Objectives:
</strong></span></strong>
                                                                            </p></div>
                                                                    </div>
                                                                </h2>
                                                            </div>
                                                        </div>

                                                        <div class="block-text__row">
                                                            <div class="block-text__col brand--body brand--linkColor">
                                                                <div class="fr-view">
                                                                    <p><em><span
                                                                                style="font-size:18px; white-space: pre">1- improve the efficiency of data users at all levels.
2- Increase the quality of data collected and processed.
3- improve the quality of the information extracted from the data and ways of delivering it.
4- Increase the speed of producing reports and final information products.
5- Awareness about the need to manage information and data.
6- Awareness about the advanced services provided by the Information Management Department to support decision-making.

                                                                            </span></em></p>
                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div>
                                                    <div class="block-text__container padeng"
                                                         data-ba="blocks.blockText">
                                                        <div class="block-text__row">
                                                            <div class="block-text__col brand--head ">

                                                                <h2>
                                                                    <div class=" brand--linkColor">
                                                                        <div class="fr-view"><p><strong><span
                                                                                        style="font-size:24px;color:rgb(49, 53, 55);"><strong>Content:

</strong></span></strong>
                                                                            </p></div>
                                                                    </div>
                                                                </h2>
                                                            </div>
                                                        </div>

                                                        <div class="block-text__row">
                                                            <div class="block-text__col brand--body brand--linkColor">
                                                                <div class="fr-view">
                                                                    <p><em><span
                                                                                style="font-size:18px; white-space: pre-line">
                                                                                SARC Data Literacy training delivers basic knowledge in the following IM areas.
                                                                                First, they start with a general understanding of IM concept and their role in data flow in the organization.
                                                                                Then, learn how to collect data and for what purpose along with different tools used from an excel table to ODK mobile data collection.
After creating data collection forms and cleaning the data collected,participants will analyze the data collected ( two prepared scenarios) to answer specific indicators considering the scenario that was given.
                                                                                Finally, they put the outcome in a report with infographics and present it to the trainers to discuss.

·         IM introduction
·         Data Collection
·         ODK Mobile data collection
·         Data Management
·         Data Analysis
·         GIS
·         Data Visualization
·         Reporting
·         MIS
·         Information sharing



                                                                            </span></em></p>
                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div>
                                                    <div class="block-text__container padeng"
                                                         data-ba="blocks.blockText">
                                                        <div class="block-text__row">
                                                            <div class="block-text__col brand--head ">

                                                                <h2>
                                                                    <div class=" brand--linkColor">
                                                                        <div class="fr-view"><p><strong><span
                                                                                        style="font-size:24px;color:rgb(49, 53, 55);"><strong>Training content:

</strong></span></strong>
                                                                            </p></div>
                                                                    </div>
                                                                </h2>
                                                            </div>
                                                        </div>

                                                        <div class="block-text__row">
                                                            <div class="block-text__col brand--body brand--linkColor">
                                                                <div class="fr-view">
                                                                    <p><em><span
                                                                                style="font-size:18px; white-space: pre-line">
                   <a href="https://docs.google.com/presentation/d/1eGcbYdUY-WCwdipCt-Ua4wrZxg68VGpNB92sYP6eE_c/edit?usp=sharing">Download IM process</a>



                                                                            </span></em></p>
                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!--/ Total Revenue -->
                    </div>
                    <a  href="/IM_introduction" >
                    <button  class="button button2">NEXT</button>
                    </a>
                </div>

                <!-- / Content -->

                <!-- Footer -->
                <!-- / Footer -->

                <div class="content-backdrop fade">

                </div>
            </div>
            <!-- Content wrapper -->
        </div>
        <!-- / Layout page -->
    </div>

    <!-- Overlay -->
    <div class="layout-overlay layout-menu-toggle"></div>
</div>
<!-- / Layout wrapper -->


<!-- Core JS -->
<!-- build:js assets/vendor/js/core.js -->
<script src="{{asset('/assets/vendor/libs/jquery/jquery.js')}}"></script>
<script src="{{asset('/assets/vendor/libs/popper/popper.js')}}"></script>
<script src="{{asset('/assets/vendor/js/bootstrap.js')}}"></script>
<script src="{{asset('/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js')}}"></script>

<script src="{{asset('/assets/vendor/js/menu.js')}}"></script>
<!-- endbuild -->

<!-- Vendors JS -->
<script src="{{asset('/assets/vendor/libs/apex-charts/apexcharts.js')}}"></script>

<!-- Main JS -->
<script src="{{asset('/assets/js/main.js')}}"></script>

<!-- Page JS -->
<script src="{{asset('/assets/js/dashboards-analytics.js')}}"></script>

<!-- Place this tag in your head or just before your close body tag. -->
<script async defer src="https://buttons.github.io/buttons.js"></script>

</body>
</html>
