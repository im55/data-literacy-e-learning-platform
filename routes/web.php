<?php

use App\Http\Controllers\admin;
use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
Route::get('/', function () {
    return view('dashboard.admin');
});

Route::get('/course', [AdminController::class,'course']);
Route::get('/IM_introduction', [AdminController::class,'show']);
Route::get('/Data_Collection', [AdminController::class,'download']);
Route::post('/num', [AdminController::class,'num'])->name('num');
//Route::get('/getAll', [App\Http\Controllers\FrontendController::class, 'getAll']);
