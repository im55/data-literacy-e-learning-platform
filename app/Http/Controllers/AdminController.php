<?php

namespace App\Http\Controllers;




use App\Models\User;

class AdminController extends Controller
{
    public function show()
    {
        $user=User::query()->findOrFail('1');
        return view('dashboard.video',compact('user'));
    }

    public function download()
    {
//       dd('d');
        return view('dashboard.download');
    }

    public function course()
    {
        $var = "off";
        return view('dashboard.course',compact('var'));
    }
    public function num (){
        $user=User::query()->findOrFail('1');

        $user->update(['num' => '1']);
        $user->save();
        return back();

    }

}
